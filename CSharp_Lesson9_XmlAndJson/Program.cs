﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Lesson9_XmlAndJson
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person
            {
                Id = 1,
                FullName = "Иван Иванович Иванов",
                BirthDate = null
            };

            List<Person> people = new List<Person>
            {
                person
            };

        }
    }
}
